import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

export const fetchProducts = createAsyncThunk('products/fetchProducts', async () => {
  
  const response = await fetch('/product.json'); 
  const data = await response.json();
  return data;
});
const initialState = {
  items: JSON.parse(localStorage.getItem('products')) || [],
  status: 'idle',
};
const productSlice = createSlice({
  name: 'products',
  initialState,
  
  reducers: {
    addToCart: (state, action) => {
      const product = state.items.find(item => item.id === action.payload);
      if (product) product.inCart = true;
     
      
    },
    removeFromCart: (state, action) => {
      const product = state.items.find(item => item.id === action.payload);
      if (product) product.inCart = false;
      
    },
    addToFavorites: (state, action) => {
      const product = state.items.find(item => item.id === action.payload);
      if (product) product.inFavorites = true;
      
    },
    removeFromFavorites: (state, action) => {
      const product = state.items.find(item => item.id === action.payload);
      if (product) product.inFavorites = false;
      
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.items = action.payload;
      })
      .addCase(fetchProducts.rejected, (state) => {
        state.status = 'failed';
      });
  }
});

export const { addToCart, removeFromCart, addToFavorites, removeFromFavorites } = productSlice.actions;

export default productSlice.reducer;

