import { createSlice } from '@reduxjs/toolkit';

const modalSlice = createSlice({
  name: 'modal',
  initialState: {
    isAddModalOpen: false,
    isRemoveModalOpen: false,
    currentProductId: null,
  },
  reducers: {
    openAddModal: (state, action) => {
      state.isAddModalOpen = true;
      state.currentProductId = action.payload;
      
    },
    closeAddModal: (state) => {
      state.isAddModalOpen = false;
      state.currentProductId = null;
    },
    openRemoveModal: (state, action) => {
      state.isRemoveModalOpen = true;
      state.currentProductId = action.payload;
    },
    closeRemoveModal: (state) => {
      state.isRemoveModalOpen = false;
      state.currentProductId = null;
    },
  },
});

export const { openAddModal, closeAddModal, openRemoveModal, closeRemoveModal } = modalSlice.actions;

export default modalSlice.reducer;
