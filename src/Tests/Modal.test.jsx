import AddModal from "../components/AddModal";
import { render, screen,fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'



describe('AddModal Component', () => {
  const mockOnClose = jest.fn();
  const mockOnConfirm = jest.fn();

  test('renders modal when isOpen is true', () => {
    render(
      <AddModal isOpen={true} onClose={mockOnClose} onConfirm={mockOnConfirm}>
        <p>Add this item to the cart?</p>
      </AddModal>
    );

    expect(screen.getByText('Add this item to the cart?')).toBeInTheDocument();
    expect(screen.getByText('Yes')).toBeInTheDocument();
    expect(screen.getByText('No')).toBeInTheDocument();
  });

  test('does not render modal when isOpen is false', () => {
    render(
      <AddModal isOpen={false} onClose={mockOnClose} onConfirm={mockOnConfirm}>
        <p>Add this item to the cart?</p>
      </AddModal>
    );

    expect(screen.queryByText('Add this item to the cart?')).not.toBeInTheDocument();
  });

  test('calls onClose when No button is clicked', () => {
    render(
      <AddModal isOpen={true} onClose={mockOnClose} onConfirm={mockOnConfirm}>
        <p>Add this item to the cart?</p>
      </AddModal>
    );

    fireEvent.click(screen.getByText('No'));
    expect(mockOnClose).toHaveBeenCalledTimes(1);
  });

  test('calls onConfirm when Yes button is clicked', () => {
    render(
      <AddModal isOpen={true} onClose={mockOnClose} onConfirm={mockOnConfirm}>
        <p>Add this item to the cart?</p>
      </AddModal>
    );

    fireEvent.click(screen.getByText('Yes'));
    expect(mockOnConfirm).toHaveBeenCalledTimes(1)
  });

 // Snapshot----------------------------------------------------------------------

 test('renders modal when isOpen is true', () => {
  render(
    <AddModal isOpen={true} onClose={mockOnClose} onConfirm={mockOnConfirm}>
      <p>Add this item to the cart?</p>
    </AddModal>
  );

  expect(screen.getByText('Add this item to the cart?')).toMatchSnapshot();
  });
});
