import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import { Button } from "../components/Button/Button";


describe('BtnProductCard Component', () => {
    test('renders button with correct text - Add to Cart', () => {
      render(< Button>Add to Cart</ Button>);
      const button = screen.getByText('Add to Cart');
      expect(button).toBeInTheDocument();
    });
  
    test('renders button with correct text - Remove from Cart', () => {
      render( <Button >Remove from Cart</ Button>);
      const button = screen.getByText('Remove from Cart');
      expect(button).toBeInTheDocument();
    });
  
    test('renders button with correct text favorites ', () => {
      render(< Button>★</ Button>);
      const button = screen.getByText('★');
      expect(button).toBeInTheDocument();
    });
  
    test('renders button with correct text favorites', () => {
      render(< Button>☆</ Button>);
      const button = screen.getByText('☆');
      expect(button).toBeInTheDocument();
    });

    test ('renders button modal text', () =>{
        render(< Button>Yes</ Button>);
        const buttonModal = screen.getByText('Yes');
        expect(buttonModal).toBeInTheDocument;
    } )
    test ('renders button modal text', () =>{
        render(< Button>No</ Button>);
        const buttonModal = screen.getByText('No');
        expect(buttonModal).toBeInTheDocument;
    } )

    test ('renders button form text', () =>{
        render(<Button>Info</Button>);
        const buttonForm = screen.getByText('Info');
        expect(buttonForm).toBeInTheDocument;
    })
    
    // Snapshot----------------------------------------------------------------------

    test ('renders button form text', () =>{
      render(<Button>Info</Button>);
      const buttonForm = screen.getByText('Info');
      expect(buttonForm).toMatchSnapshot();
  })
 
   
});


