import productSlice,{addToCart, removeFromCart, addToFavorites, removeFromFavorites} from "../ReduxComponents/ProductSlice";

describe('productSlice', () => {
    const initialState ={
        items : [
            { id:1, name:'Test Product 1 ', inCart: false, inFavorites: false }
        ],
        status: 'idle',
    }

    test ('Should handle addCart', ()=> {
        const state =productSlice(initialState, addToCart(1));
        expect(state.items[0].inCart).toBe(true);
    })

    test('should handle removeFromCart', () =>{
        const state = {
            ...initialState,
            items:[{id:1, name:'Test Product 1 ', inCart: false, inFavorites: false }],
        }
        const newState = productSlice(state,removeFromCart(1))
        expect(newState.items[0].inCart).toBe(false);
    }) 
    
    test('should handle addToFavorites', () => {
        const state =  productSlice(initialState, addToFavorites(1));
        expect(state.items[0].inFavorites).toBe(true);
      });
    
      test('should handle removeFromFavorites', () => {
        const state = {
          ...initialState,
          items: [{ id: 1, name: 'Test Product 1', inCart: false, inFavorites: true }],
        };
        const newState =  productSlice(state, removeFromFavorites(1));
        expect(newState.items[0].inFavorites).toBe(false);
      });
    

    
})