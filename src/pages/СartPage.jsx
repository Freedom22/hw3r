 import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import ProductCart from '../components/ProductCart';
import"./FavCartPage.css"
import UsersForm from '../components/UsersInfo';

function CartPage() {
 

  const products = useSelector((state) => state.products.items);
  

  const cartItems = useMemo(() => 
    products.filter((item) => item.inCart), [products]);
   
       
      return (
        <div className='favcart'>
        {cartItems.length > 0 ? (
          cartItems.map((product) => (
            <ProductCart key={product.id} product={product} />
          ))
        ) : (
          <p>Your cart is empty</p>
        )}
        <UsersForm />
      </div>
    );
  }
  
  export default CartPage;
