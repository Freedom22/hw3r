 
import"./FavCartPage.css"

import { useMemo } from 'react';;
import { useSelector } from 'react-redux';
import ProductCart from '../components/ProductCart';

function FavoritesPage() {
  const products = useSelector((state) => state.products.items);

  const favoriteItems = useMemo(() => 
    products.filter((item) => item.inFavorites), [products]);

  return (
    <div className='favcart'>
    {favoriteItems.length > 0 ? (
      favoriteItems.map((product) => (
        <ProductCart key={product.id} product={product} />
      ))
    ) : (
      <p>Your favorites list is empty</p>
    )}
  </div>
);
}

export default FavoritesPage;
