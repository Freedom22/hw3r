// store.js
import { configureStore } from '@reduxjs/toolkit';
import productReducer from './ReduxComponents/ProductSlice';
import modalReducer from './ReduxComponents/ModalSlice';

const store = configureStore({
  reducer: {
    products: productReducer,
    modal: modalReducer
  },
});

export default store;
