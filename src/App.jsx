
import { BrowserRouter as Router, Route,  Routes } from 'react-router-dom';
// import { GlobalProvider } from './context/GlobalContext';
import Header from './Header';
import ProductList from './components/ProductList';
import CartPage from './pages/СartPage';
import FavoritesPage from './pages/FavoritesPage';
import { Provider } from 'react-redux';
import store from './store';
import { ProductViewProvider } from './components/ModeContext';


function App() {
  return (
    <Provider store={store}>
      <ProductViewProvider>
    {/* <GlobalProvider> */}
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<ProductList />} />
          <Route path="/cart" element={<CartPage />} />
          <Route path="/favorites" element={<FavoritesPage />} />
        </Routes>
      </Router>
    {/* </GlobalProvider> */}
    </ProductViewProvider>
    </Provider>
  );
}


export default App;
