
import { Link } from 'react-router-dom';
import './Header.css'
import { useSelector } from 'react-redux';

function Header() {
  

  const cartItemsCount = useSelector((state) =>
  state.products.items.filter((item) => item.inCart).length
);

const favoriteItemsCount = useSelector((state) =>
  state.products.items.filter((item) => item.inFavorites).length
);

  return (
    <header className='header'>
      <nav className='header__nav'>
        <ul className='ul'>
          <li className='items'>
            <Link to="/">Home</Link>
          </li>
          <li className='items'>
            <Link to="/cart">Cart ({cartItemsCount})</Link>
          </li>
          <li className='items'>
            <Link to="/favorites">Favorites ({favoriteItemsCount})</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
