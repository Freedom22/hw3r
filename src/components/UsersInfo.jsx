import  { useState } from 'react';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import "./UsersInfo.css"
import { Button } from './Button/Button';


function UsersForm (){

  const [isInfoOpen, setIsInfoOpen] = useState(false);
  
  const ItemsInCart = createSelector(
    state => state.products.items,
    items => items.filter(item => item.inCart)
  )

  const AddItems = useSelector(ItemsInCart);

  const handleButtonClick = () => {
    setIsInfoOpen(!isInfoOpen);
  };

    const Info = {
        firstName: '',
        lastName: '',
        age: '',
        address: '',
        phone: '',
      };

      const ValidationUsers = Yup.object().shape({
        firstName: Yup.string().required('Ім\'я є обов\'язковим полем'),
        lastName: Yup.string().required('Прізвище є обов\'язковим полем'),
        age: Yup.number()
          .typeError('Введіть число')
          .positive('Вік має бути позитивним числом')
          .integer('Вік має бути цілим числом')
          .required('Вік є обов\'язковим полем'),
        address: Yup.string().required('Адреса доставки є обов\'язковим полем'),
        phone: Yup.string()
        .matches(/^[0-9]+$/, 'Номер телефону має містити тільки цифри')
        .required('Мобільний телефон є обов\'язковим полем'),
      });

      const handleSubmit = (values, { resetForm }) => {
      
        console.log('Придбані товари:',AddItems);

        console.log('Інформація про користувача:', values);
       
        
        resetForm();
      };
     
    return(
      <div>
      {AddItems.length > 0 ? (
        <div>
          <Button onClick={handleButtonClick} className='info__btn'>Info</Button>
          {isInfoOpen && (
            <div className='user-form-container'>
              <h2>Оформлення замовлення</h2>
              <Formik
                initialValues={Info}
                validationSchema={ValidationUsers}
                onSubmit={handleSubmit}
              >
                {({ errors, touched }) => (
                  <Form className='user-form'>
                    <div className="form-group">
                      <label htmlFor="firstName">Ім'я:</label>
                      <Field type="text" id="firstName" name="firstName" className="input__info" />
                      <ErrorMessage name="firstName" component="div" className="error" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="lastName">Прізвище:</label>
                      <Field type="text" id="lastName" name="lastName" className="input__info" />
                      <ErrorMessage name="lastName" component="div" className="error" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="age">Вік:</label>
                      <Field type="text" id="age" name="age" className="input__info" />
                      <ErrorMessage name="age" component="div" className="error" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="address">Адреса доставки:</label>
                      <Field type="text" id="address" name="address" className="input__info" />
                      <ErrorMessage name="address" component="div" className="error" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="phone">Мобільний телефон:</label>
                      <Field type="text" id="phone" name="phone" className="input__info" />
                      <ErrorMessage name="phone" component="div" className="error" />
                    </div>
                    <button type="submit" className='checkout'>Checkout</button>
                  </Form>
                )}
              </Formik>
            </div>
          )}
        </div>
      ) : (
        <div className="warning__text">Ваш кошик порожній. Додайте товари перед оформленням замовлення.</div>
      )}
    </div>
    )
}

export default UsersForm