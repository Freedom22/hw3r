
import "./ProductCart.css";
import AddModal from './AddModal';
import Modal from './Modal';

import { useDispatch, useSelector } from 'react-redux';
import { addToCart, removeFromCart, addToFavorites, removeFromFavorites } from '../ReduxComponents/ProductSlice';
import { openAddModal, closeAddModal, openRemoveModal, closeRemoveModal } from '../ReduxComponents/ModalSlice';
import { Button } from "./Button/Button";



function ProductCard({ product }) {
  const dispatch = useDispatch();
 

  const isAddModalOpen = useSelector((state) => state.modal.isAddModalOpen && state.modal.currentProductId === product.id);
  const isRemoveModalOpen = useSelector((state) => state.modal.isRemoveModalOpen && state.modal.currentProductId === product.id);

  const isInCart = useSelector((state) =>
    state.products.items.find((item) => item.id === product.id)?.inCart
  );

  const isInFavorites = useSelector((state) =>
    state.products.items.find((item) => item.id === product.id)?.inFavorites
  );

  const handleAddToCart = () => {
    dispatch(openAddModal(product.id));
  };

  const handleConfirmAddToCart = () => {
    dispatch(addToCart(product.id));
    dispatch(closeAddModal());
  };

  const handleRemoveFromCart = () => {
    dispatch(openRemoveModal(product.id));
  };

  const handleConfirmRemoveFromCart = () => {
    dispatch(removeFromCart(product.id));
    dispatch(closeRemoveModal());
  };

  const handleAddToFavorites = () => {
    dispatch(addToFavorites(product.id));
  };

  const handleRemoveFromFavorites = () => {
    dispatch(removeFromFavorites(product.id));
  };

  return (
    <div className="product">
      <img src={product.imageUrl} alt={product.name} className="product__image" />
      <h3>{product.type}</h3>
      <h4>{product.price}$</h4>
      <p>{product.name}</p>

      <div className="buttons">
        {isInCart ? (
          <Button  onClick={handleRemoveFromCart} >Remove from Cart</Button>
        ) : (
          <Button onClick={handleAddToCart} >Add to Cart</Button>
        )}

        {isInFavorites ? (
          <Button onClick={handleRemoveFromFavorites} className="favorite-button active">★</Button>
        ) : (
          <Button onClick={handleAddToFavorites} className="favorite-button">☆</Button>
        )}
      </div>

      <AddModal
        isOpen={isAddModalOpen}
        onClose={() => dispatch(closeAddModal())}
        onConfirm={handleConfirmAddToCart}
      >
        <p>Add this item to the cart?</p>
      </AddModal>

      <Modal
        isOpen={isRemoveModalOpen}
        onClose={() => dispatch(closeRemoveModal())}
        onConfirm={handleConfirmRemoveFromCart}
      >
        <p>Remove this item from the cart?</p>
      </Modal>
    </div>
  );
}

export default ProductCard;