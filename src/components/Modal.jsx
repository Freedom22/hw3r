
import './Modal.css';
import { Button } from './Button/Button';

function Modal ({isOpen,onClose, onConfirm, children}){
   
    if (!isOpen) return null;

    return(
        <div className="modal-overlay">
            <div className="modal">
              <div className="modal-content">{children}</div>
                <div className="modal-buttons">
                    <Button onClick={onConfirm}>Yes</Button>
                    <Button onClick={onClose}>No</Button>
                </div>
            </div>
        </div>
    )

}


export default Modal