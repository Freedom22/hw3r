import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from '../ReduxComponents/ProductSlice';
import ProductCard from './ProductCart';
import './ProductList.css';
import { useProductView } from './ModeContext';

function ProductList() {
  const dispatch = useDispatch(); 
  const { isTableView, toggleView } = useProductView();
  const products = useSelector((state) => state.products.items);
  const productStatus = useSelector((state) => state.products.status);

  useEffect(() => {
    if (productStatus === 'idle') {
      dispatch(fetchProducts());
    }
  }, [productStatus, dispatch]);

  const renderProductCards = () => {
    return products.map(product => (
      <ProductCard key={product.id} product={product} />
    ));
  };

  const renderProductTable = () => {
    return (
      <table className="product-table">
        <thead>
          <tr>
            <th>Назва</th>
            <th>Ціна</th>
            <th>Тип</th>
            
          </tr>
        </thead>
        <tbody>
          

          
          {products.map(product => (
            <tr key={product.id}>
              <td>{product.name}</td>
              <td>{product.price}</td>
              <td>{product.type}</td>
              
            </tr>
           
          ))}
        </tbody>
      </table>
    );
  };

  return (
    <div className="product-list">
      <div className="view-toggle">
        <button onClick={toggleView}>
          {isTableView ? 'Показати у картках' : 'Показати у таблиці'}
        </button>
      </div>
      {isTableView ? renderProductTable() : renderProductCards()}
    </div>
  );
}

export default ProductList;
