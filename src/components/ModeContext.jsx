import React, { createContext, useState, useContext } from 'react';

const ProductViewContext = createContext();

export const useProductView = () => {
  return useContext(ProductViewContext);
};

export const ProductViewProvider = ({ children }) => {
  const [isTableView, setIsTableView] = useState(false);

  const toggleView = () => {
    setIsTableView(prevState => !prevState);
  };

  return (
    <ProductViewContext.Provider value={{ isTableView, toggleView }}>
      {children}
    </ProductViewContext.Provider>
  );
};

export default ProductViewContext;
