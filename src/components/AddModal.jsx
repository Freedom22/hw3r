import './AddModal.css'
import { Button } from './Button/Button';


function AddModal  ({isOpen,onClose, onConfirm, children}){
    if (!isOpen) return null;
    return(
        <div className='modal_add'>
            <div className="modal__content">
            <div className="modal-content">{children}</div>
            <div className="modal-buttons">
                    <Button onClick={onConfirm}>Yes</Button>
                    <Button onClick={onClose}>No</Button>
                </div>
            </div>
        </div>
    )
}

export default AddModal